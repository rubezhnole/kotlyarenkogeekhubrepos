import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class OutHelper {

    private File currentDirectory = null;

    public OutHelper() {
        currentDirectory = new File(System.getProperty("user.dir"));
    }

    public File getCurrentDirectory() {
        return currentDirectory;
    }

    public void setCurrentDirectory(File currentDirectory) {
        currentDirectory = currentDirectory;
    }

    public void showDirectory(PrintWriter out) {
        File[] files = getCurrentDirectory().listFiles();

        for (int i = 0; i < files.length; i++) {

            if (files[i].isFile()) {
                out.println("<a class='href' href='index?do=openFile&id=" + i + "'>" +
                        files[i].getName() + " file<input name='do' type='hidden'  value='open'></a>" +
                        "<form><input type='submit' method='get' action='TestServlet' value='delete'>" +
                        "<input name='id' type='hidden'  value='" + i + "'>" +
                        "<input name='do' type='hidden'  value='delete'></form>");
            } else if (files[i].isDirectory()) {
                out.println("<a href=" + files[i].getPath() + ">" + files[i].getName() + " directory</a>" +
                        "<form><input type='submit' method='get' action='TestServlet' value='delete'>" +
                        "<input name='id' type='hidden'  value='" + i + "'>" +
                        "<input name='do' type='hidden'  value='delete'></form>" +
                        "<form><input type='submit' method='get' action='TestServlet' value='open directory'>" +
                        "<input name='do' type='hidden'  value='openDirectory'>" +
                        "<input name='path' type='hidden'  value='" + files[i].getPath() + "'></form><hr>");    // поменять путь файла
            }
        }
    }

    public boolean deleteFile(String id) {
        File[] files = currentDirectory.listFiles();
        int idInt = Integer.parseInt(id);

        System.out.println("files " + files[idInt].getName() + " will be delete.");

        return files[idInt].delete();
    }

    public boolean createFile(String title) {
        String path = getCurrentDirectory().getPath();
        File file = new File(path + File.separator + title + ".txt");

        System.out.println(file.getPath() + " path where create file");

        try {
            return file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public void showParentDirectory(PrintWriter out) {
        currentDirectory = new File(currentDirectory.getParent());

    }

    public void showCurrentDirectory(PrintWriter out) {
        out.println("<a href=" + getCurrentDirectory().getPath() + ">" + getCurrentDirectory().getPath() + "</a><br>");
    }

    public void manageMethods(String doPar, String idPar, PrintWriter out, String title, String path) {

        try {
            if (doPar.equals("delete")) {
                deleteFile(idPar);
            } else if (doPar.equals("back")) {
                showParentDirectory(out);
            } else if (doPar.equals("create")) {
                createFile(title);
            } else if (doPar.equals("openDirectory")) {
                currentDirectory = new File(path);
            } else if (doPar.equals("openFile")) {
                openFile(out, idPar);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void openFile(PrintWriter out, String id) {

        File[] files = currentDirectory.listFiles();
        int idInt = Integer.parseInt(id);
        String path = files[idInt].getPath();
        System.out.println(path + " path file to open");

        String s;
        StringBuilder sb = new StringBuilder();

        try (BufferedReader in = new BufferedReader(new FileReader(path))) {
            while ((s = in.readLine()) != null) {
                sb.append(s).append("<br>");
            }

            System.out.println(sb);
            out.println("open file: " + path);
            out.println("<p>" + sb + "</p>");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
