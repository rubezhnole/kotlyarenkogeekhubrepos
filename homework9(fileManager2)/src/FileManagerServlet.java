import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class FileManagerServlet extends HttpServlet {

    OutHelper helper = null;

    public FileManagerServlet() {
        super();
        helper = new OutHelper();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("text/html");

        PrintWriter out = resp.getWriter();

        String idPar = req.getParameter("id");
        String doPar = req.getParameter("do");
        String title = req.getParameter("title");
        String path = req.getParameter("path");

        System.out.println(idPar + " idPar parameter");
        System.out.println(doPar + " doPar parameter");
        System.out.println(title + " title parameter");
        System.out.println(path + " path parameter");

        helper.manageMethods(doPar, idPar, out, title, path);

        out.println("<html><head><title>File manager</title><meta http-equiv='content-type'  " +
                "content='text/html; charset=utf-8'></head><body>");
        out.println("<form method='get' action='index'>Name: <input type='text' name='title' /><button>new file</button>" +
                "<input name='do' type='hidden'  value='create'></form>");
        out.println("<form method='get' action='index'><button>back</button>" +
                "<input name='do' type='hidden'  value='back'></form>");

        helper.showCurrentDirectory(out);

        out.println("<hr><hr>");

        helper.showDirectory(out);

        out.println("</body></html>");
        out.close();
    }
}
// открытие файла по клику на ссылку