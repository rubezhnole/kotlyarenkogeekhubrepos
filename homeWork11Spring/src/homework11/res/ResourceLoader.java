package homework11.res;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Map;
import java.util.TreeMap;

@Component
public class ResourceLoader {

    private String pathToTextSource;
    private String pathToDictionary;

    @Autowired
    public ResourceLoader(@Value("E:\\javaForWeb\\kotlyarenkogeekhubrepos\\homeWork11Spring\\src\\homework11\\text\\text.txt") String pathToTextSource,
                          @Value("E:\\javaForWeb\\kotlyarenkogeekhubrepos\\homeWork11Spring\\src\\homework11\\dic\\dictionary.txt") String pathToDictionary) {
        this.pathToTextSource = pathToTextSource;
        this.pathToDictionary = pathToDictionary;
    }

    public String loadTextResource(){

        String s;
        StringBuilder sb = new StringBuilder();

        try (BufferedReader in = new BufferedReader(new FileReader(pathToTextSource))) {

            while ((s = in.readLine()) != null) {
                sb.append(s).append("\n");
            }

            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public Map<String, String> loadDictionaryResource(){

        String s;
        Map<String, String> dic = new TreeMap<>();

        try (BufferedReader in = new BufferedReader(new FileReader(pathToDictionary))) {

            while ((s = in.readLine()) != null) {
                String[] split = s.split("/");
                dic.put(split[0], split[1]);
            }

            return dic;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

