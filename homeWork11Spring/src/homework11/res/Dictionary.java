package homework11.res;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.TreeMap;

@Component
public class Dictionary {

    private Map<String, String> dictionary;
    @Autowired
    private ResourceLoader resourceLoader;

    public Dictionary() {
        this.dictionary = new TreeMap<>();
    }

    public Map<String, String> loadDictionary() {
        Map<String, String> dic = resourceLoader.loadDictionaryResource();

        return dictionary = dic;
    }

    public void showDictionary(){

        for (Map.Entry<String, String> map : dictionary.entrySet()) {
            System.out.println(map.getKey() + " " + map.getValue());
        }
    }
}