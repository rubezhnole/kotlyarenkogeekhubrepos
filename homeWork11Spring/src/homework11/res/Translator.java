package homework11.res;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class Translator {

    @Autowired
    private TextSource textSource;
    @Autowired
    private Dictionary dictionary;
    @Autowired
    private LanguageDetector languageDetector;

    public Translator() {
    }

    public String translete() {

        final String[] textForTranslete = {textSource.prepereText()};

        System.out.println(textForTranslete[0]);

        Map<String, String> textForDictionary = dictionary.loadDictionary();
        String defineLanguage = languageDetector.defineLanguage();

        if (defineLanguage.equals("en")) {

            textForDictionary.entrySet().stream()
                    .filter(map -> textForTranslete[0].contains(map.getKey()))
                    .forEach(map -> textForTranslete[0] = textForTranslete[0]
                            .replace(map.getKey(), map.getValue()));
        }

        return textForTranslete[0];
    }
}
