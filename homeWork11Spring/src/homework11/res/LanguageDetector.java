package homework11.res;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LanguageDetector {

    @Autowired
    private ResourceLoader resourceLoader;

    public LanguageDetector() {
    }

    public String defineLanguage() {//97 - 122 en
        String text = resourceLoader.loadTextResource();

        for (int i = 97; i < 123; i++) {
            String tmp = String.valueOf((char) i);

            if (text.contains(tmp)) {
                return "en";
            }
        }

        return null;
    }
}