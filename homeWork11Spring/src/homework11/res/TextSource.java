package homework11.res;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class TextSource {

    @Autowired
    ResourceLoader resourceLoader;

    public TextSource() {
    }

    public String prepereText() {
        String content = resourceLoader.loadTextResource();
        content = content.replaceAll("<(.)+?>", "");
        content = content.replaceAll("<(\n)+?>", "");

        return content;
    }

    public Map<String, String> prepereDictionary() {
        Map<String, String> content = resourceLoader.loadDictionaryResource();

        for (Map.Entry<String, String> map : content.entrySet()) {
            content.put(map.getKey().replaceAll("<(.)+?>", ""), map.getValue().replaceAll("<(.)+?>", ""));
        }

        return content;
    }
}