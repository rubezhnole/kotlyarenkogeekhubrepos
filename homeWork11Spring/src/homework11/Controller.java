package homework11;

import homework11.res.Translator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Controller {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("homework11/config.xml");
        Translator translator = (Translator) context.getBean("translator");

        System.out.println(translator.translete());
    }
}