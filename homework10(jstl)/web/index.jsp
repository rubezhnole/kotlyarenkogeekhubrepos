<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/customTags/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="WEB-INF/css/style.css"/>
    <title>Pagination</title>
</head>
<body>
    <c:set var="cP" value="${param.page}"/>

    <c:if test="${param.page == null}">
        <c:set var="cP" value="1"/>
    </c:if>

    <c:set var="rPP" value="10"/>
    <c:set var="tR" value="125"/>
    <c:set var="a"/>
    <c:set var="b" value="${tR / rPP}"/>

    <c:choose>
        <c:when test="${a == 0}">
            <c:set var="numberOfPages" value="1" scope="session"/>
        </c:when>

        <c:when test="${b > a}">
            <c:set var="xxx" value="${b % a}"/>
            <c:if test="${xxx > 0}">
                <c:set var="numberOfPages" value="${b - xxx + 1}" scope="session"/>
            </c:if>
        </c:when>

        <c:when test="${a >= b}">
            <c:set var="numberOfPages" value="${a}" scope="session"/>
        </c:when>
    </c:choose>

    <c:set var="start" value="${cP * rPP - rPP}"/>
    <c:set var="stop" value="${cP * rPP - 1}"/>

    <table border="1" align="center">
        <c:forEach items="${requestScope.person}" var="emp" begin="${start}" end="${stop}">
            <tr>
                <td><c:out value="${emp.id}"/></td>
                <td><c:out value="${emp.name}"/></td>
            </tr>
        </c:forEach>
    </table>

    <br>

    <div align="center">
        <c:out value="${start + 1}"/> - <c:out value="${stop + 1}"/> / <c:out value="${tR}"/>
    </div>

    <ct:pagination resultsPerPage="${rPP}" totalResults="${tR}" currentPage="${cP}" pageURL="test"/>

</body>
</html>
