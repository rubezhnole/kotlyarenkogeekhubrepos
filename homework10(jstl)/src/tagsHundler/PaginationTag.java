package tagsHundler;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

public class PaginationTag extends BodyTagSupport {

    private String resultsPerPage;
    private String totalResults;
    private String currentPage;
    private String pageURL;

    public void setResultsPerPage(String resultsPerPage) {
        this.resultsPerPage = resultsPerPage;
    }

    public void setTotalResults(String totalResults) {
        this.totalResults = totalResults;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public void setPageURL(String pageURL) {
        this.pageURL = pageURL;
    }

    public String getResultsPerPage() {
        return resultsPerPage;
    }

    public String getTotalResults() {
        return totalResults;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public String getPageURL() {
        return pageURL;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            int resPerPageInt = Integer.parseInt(resultsPerPage);
            int totalResultInt = Integer.parseInt(totalResults);
            int currentPageInt = Integer.parseInt(currentPage);

            JspWriter out = pageContext.getOut();

            System.out.println(getResultsPerPage() + " " + getTotalResults() + " " + getCurrentPage() + " " + getPageURL());

            if (currentPage.equals("") || currentPageInt == 0) {
                currentPage = "1";
                currentPageInt = 1;
            }

            System.out.println(resultsPerPage + " " + totalResults + " " + currentPage + " " + pageURL);

            out.print("<div align='center'>");

            if (currentPageInt != 1 && currentPageInt != (totalResultInt / resPerPageInt)) {

                out.print("<a href='indexx?page=" + (currentPageInt - 1) + "'><strong> < </strong></a> ");

                for (int i = 1; i <= totalResultInt / resPerPageInt; i++) {

                    if (currentPageInt == i) {
                        out.print("<a href='indexx?page=" + i + "'><strong>" + i + "</strong></a> ");
                    } else {
                        out.print("<a href='indexx?page=" + i + "'>" + i + "</a> ");
                    }
                }

                out.print("<a href='indexx?page=" + (currentPageInt + 1) + "'><strong> > </strong></a> ");
            } else if (currentPageInt == 1) {

                for (int i = 1; i <= totalResultInt / resPerPageInt; i++) {
                    if (currentPageInt == i) {
                        out.print("<a href='indexx?page=" + i + "'><strong>" + i + "</strong></a> ");
                    } else {
                        out.print("<a href='indexx?page=" + i + "'>" + i + "</a> ");
                    }
                }

                out.print("<a href='indexx?page=" + (currentPageInt + 1) + "'><strong> > </strong></a> ");
            } else if (currentPageInt == (totalResultInt / resPerPageInt)) {
                out.print("<a href='indexx?page=" + (currentPageInt - 1) + "'><strong> < </strong></a> ");

                for (int i = 1; i <= totalResultInt / resPerPageInt; i++) {

                    if (currentPageInt == i) {
                        out.print("<a href='indexx?page=" + i + "'><strong>" + i + "</strong></a> ");
                    } else {
                        out.print("<a href='indexx?page=" + i + "'>" + i + "</a> ");
                    }
                }
            }

            out.print("<div>");

        } catch (Exception e) {
            throw new JspException("Error: " + e.getMessage());
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}
/*Page Navigation tag

Задача тега заключается в представлении постраничной навигации разнообразного контента. В качестве примера можно
использовать отсортированный список элементов и выводить их N-е количество на страницу. Каждая страница будет иметь
свой уникальный адрес, пример http://localhost/people?page=4 Если параметр page не указан, то по умолчанию отображаем
 первую страницу. При переходе на определенную страницу мы должны отображать соответствующую часть списка. В качестве
 параметра в тег нужно передавать следующие параметры:
resultsPerPage – количество элементов которые выводятся на страницу;
totalResults – общее количество результатов;
currentPage – текущая страница;
pageURL – URL для перехода на определенную страницу, в которой нужно использовать метку {page} в том месте где должен
быть указан параметр страницы.
Так для текущего примера тэг будет выглядеть примерно так
<gh:pagination resultsPerPage=”10” totalResults=”82” currentPage=”4” pageURL=”people?page={page}”/>
Результатом должен быть аккуратно оформлен элемент управлением постраничной навигации. В верхней части отображаем номер
первого и последнего элемента отображаемой части списка и общее количество результатов.
Ниже отображаем перечень всех доступных страниц в виде номеров с ссылкой на соответствующую страницу. Текущая страница
должна подсвечиваться. Стрелки в начале и конце списка страниц должны «перелистывать» на предыдущую/следующую страницы.
 Если мы находимся на первой странице стрелка в начале списка не должна отображаться, аналогично поступаем и
 со стрелкой в конце списка страниц.

Список результатов не является частью тэга.﻿*/