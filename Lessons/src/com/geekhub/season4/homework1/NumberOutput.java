package com.geekhub.season4.homework1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class NumberOutput {

    public static String outMass(int n) {
        String numberMass[] = new String[10];
        numberMass[0] = "Ноль";
        numberMass[1] = "Один";
        numberMass[2] = "Два";
        numberMass[3] = "Три";
        numberMass[4] = "Четыри";
        numberMass[5] = "Пять";
        numberMass[6] = "Шесть";
        numberMass[7] = "Семь";
        numberMass[8] = "Восемь";
        numberMass[9] = "Девять";

        return numberMass[n];
    }

    public static String outSwitch(int n) {
        String strNumbSwitch = null;

        switch (n) {
            case 0:
                strNumbSwitch = "Ноль";
                break;
            case 1:
                strNumbSwitch = "Один";
                break;
            case 2:
                strNumbSwitch = "Два";
                break;
            case 3:
                strNumbSwitch = "Три";
                break;
            case 4:
                strNumbSwitch = "Четыри";
                break;
            case 5:
                strNumbSwitch = "Пять";
                break;
            case 6:
                strNumbSwitch = "Шесть";
                break;
            case 7:
                strNumbSwitch = "Семь";
                break;
            case 8:
                strNumbSwitch = "Восемь";
                break;
            case 9:
                strNumbSwitch = "Девять";
                break;
        }

        return strNumbSwitch;
    }

    public static String outIF(int n) {
        String strNumbFor = null;

        if (n == 0) {
            strNumbFor = "Ноль";
        } else if (n == 1) {
            strNumbFor = "Один";
        } else if (n == 2) {
            strNumbFor = "Два";
        } else if (n == 3) {
            strNumbFor = "Три";
        } else if (n == 4) {
            strNumbFor = "Четыри";
        } else if (n == 5) {
            strNumbFor = "Пять";
        } else if (n == 6) {
            strNumbFor = "Шесть";
        } else if (n == 7) {
            strNumbFor = "Семь";
        } else if (n == 8) {
            strNumbFor = "Восемь";
        } else if (n == 9) {
            strNumbFor = "Девять";
        }

        return strNumbFor;
    }

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int         number;
        boolean     loopAgain;

        System.out.print("Введите цифру от 0 до 9: ");

        do {

            try {
                number = Integer.parseInt(reader.readLine());

                if (number < 0 || number > 9) {
                    throw new IOException();
                }

                System.out.println("Вывод с помощью маcсива: " + outMass(number));
                System.out.println("Вывод с помощью switch: " + outSwitch(number));
                System.out.println("Вывод с помощью if else: " + outIF(number));

                loopAgain = false;
            } catch (IOException ex) {
                System.out.println("\nНеверное число. Попробуйте снова.\n");

                loopAgain = true;
            }

        } while (loopAgain);

    }

}
