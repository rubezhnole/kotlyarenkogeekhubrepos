package com.geekhub.season4.homework1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Factorial {

    public static double calcFactorial(double numberFactorial) {
        if (numberFactorial == 0) {
            return 1.0;
        } else {
            return Math.sqrt(2 * Math.PI * numberFactorial)
                    * Math.pow(numberFactorial / Math.E, numberFactorial);
        }
    }

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int inputNumber;
        double outcome;
        boolean againLoop;

        System.out.print("Please enter the number to calculate factorial (0 < number < 170): ");

        do {
            try {
                inputNumber = Integer.parseInt(reader.readLine());

                if (inputNumber < 0 || inputNumber > 170) {
                    throw new IOException();
                }

                outcome = Factorial.calcFactorial((double) inputNumber);
                againLoop = false;

                System.out.println("Approximate factorial of a number " + inputNumber + " = " + outcome);
            } catch (NumberFormatException NFEx) {
                System.out.println("\nError! You typed literal!\n");

                againLoop = true;
            } catch (IOException IOEx) {
                System.out.println("\nError! Wrong number!\n");

                againLoop = true;
            }

            System.out.print("Please enter the number to calculate factorial (0 < number < 170): ");
        } while (againLoop);

    }

}
