package com.geekhub.season4.homework1;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class Fibonacci {

    public static void calcFibonacci(int n) {
        BigInteger resultFibFirst = new BigInteger("1");
        BigInteger resultFibSecond = new BigInteger("1");
        BigInteger resultFib;

        System.out.print(resultFibFirst + " " + resultFibSecond);

        for (int i = 2; i < n; i++) {
            resultFib = resultFibFirst.add(resultFibSecond);
            resultFibFirst = resultFibSecond;
            resultFibSecond = resultFib;

            System.out.print(" " + resultFib);
        }
    }

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int number;

        System.out.print("Введите число для определения последовательности Фибоначчи: ");

        try {
            number = Integer.parseInt(reader.readLine());

            calcFibonacci(number);
        } catch (Exception ex) {
            System.out.println("\nОшибка.\n");
        }

    }

}
