package com.geekhub.season4.homework2;

public class Chassis {

    String gear;

    public Chassis() {
        this.gear = "Rear drive";
    }

    @Override
    public String toString() {
        return " gear: " + gear;
    }

}
