package com.geekhub.season4.homework2;

public class TestClass {

    public static void main(String[] args) {

        Car car = new Car(200, 500, 300);         // car

        System.out.println(car.toString());

        car.accelerate(15);

        System.out.println(car.toString());

        car.turnRight(14);

        System.out.println(car.toString());

        car.turnLeft(16);

        System.out.println(car.toString());

        car.brake(20);

        System.out.println(car.toString());

        Boat boat = new Boat(120, 100, 160);       // boat

        System.out.println(boat.toString());

        boat.accelerate(45);

        System.out.println(boat.toString());

        boat.turnLeft(12);

        System.out.println(boat.toString());

        boat.turnRight(45);

        System.out.println(boat.toString());

        SolarPoweredCar solarCar = new SolarPoweredCar(200, 100, 250);        // solar car

        System.out.println(solarCar.toString());

        solarCar.accelerate(20);

        System.out.println(solarCar.toString());

        solarCar.turnLeft(14);

        System.out.println(solarCar.toString());

        solarCar.brake(15);

        System.out.println(solarCar.toString());

        solarCar.turnRight(23);

        System.out.println(solarCar.toString());

        solarCar.accelerate(14);

        System.out.println(solarCar.toString());

    }

}
