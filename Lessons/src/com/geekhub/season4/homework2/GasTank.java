package com.geekhub.season4.homework2;

public class GasTank implements EnergyProvider {

    int fuel;

    public GasTank(int gas) {
        this.fuel = gas;
    }

    @Override
    public void addEnergy(int energy) {
        this.fuel = fuel + energy;
    }

    @Override
    public void removeEnergy(int energy) {
        this.fuel = fuel - energy;
    }

    @Override
    public int getEnergy() {
        return fuel;
    }

    @Override
    public void setEnergy(int energy) {
        this.fuel = energy;
    }

    @Override
    public String toString() {
        return " |fuel: " + fuel;
    }

}

