package com.geekhub.season4.homework2;

public class Boat extends Vechile {

    public Boat(int force, int energy, int speed) {
        super(new DieselEngine(force), new GasTank(energy), new Propeller(speed));
    }

}
