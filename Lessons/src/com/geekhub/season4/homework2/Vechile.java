package com.geekhub.season4.homework2;

public abstract class Vechile implements Driveable {

    private ForceProvider forceProvider;
    private EnergyProvider energyProvider;
    private ForceAcceptor forceAcceptor;

    public Vechile(ForceProvider forceProvider, EnergyProvider energyProvider, ForceAcceptor forceAcceptor) {
        this.forceProvider = forceProvider;
        this.energyProvider = energyProvider;
        this.forceAcceptor = forceAcceptor;
    }

    @Override
    public void accelerate(int pressThePedal) {
        forceAcceptor.aligin();
        forceAcceptor.addSpeed((energyProvider.getEnergy() / 2) / pressThePedal);
        forceProvider.addForce(forceAcceptor.getSpeed() / pressThePedal);
        energyProvider.removeEnergy(forceAcceptor.getSpeed() / forceProvider.getForce());
    }

    @Override
    public void brake(int pressThePedal) {
        forceAcceptor.aligin();
        forceAcceptor.removeSpeed((energyProvider.getEnergy() / 2) / pressThePedal);
        forceProvider.removeForce(forceAcceptor.getSpeed() / pressThePedal);
        energyProvider.removeEnergy(forceAcceptor.getSpeed() / (forceProvider.getForce() / 2));
    }

    @Override
    public void turnLeft(int angleTurnLeft) {
        forceAcceptor.removeSpeed((energyProvider.getEnergy() / 4) / angleTurnLeft);
        forceProvider.removeForce(forceAcceptor.getSpeed() / angleTurnLeft);
        energyProvider.removeEnergy(forceAcceptor.getSpeed() / (forceProvider.getForce() / 4));
        forceAcceptor.turnLeft();
    }

    @Override
    public void turnRight(int angleTurnRight) {
        forceAcceptor.removeSpeed((energyProvider.getEnergy() / 4) / angleTurnRight);
        forceProvider.removeForce(forceAcceptor.getSpeed() / angleTurnRight);
        energyProvider.removeEnergy(forceAcceptor.getSpeed() / (forceProvider.getForce() / 4));
        forceAcceptor.turnRight();
    }

    @Override
    public String toString() {
        return forceAcceptor.toString() + forceProvider.toString() + energyProvider.toString();
    }

}
