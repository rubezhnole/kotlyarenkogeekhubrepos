package com.geekhub.season4.homework2;

public class ElectricEngine implements ForceProvider {

    int electricPower;

    public ElectricEngine(int power) {
        this.electricPower = power;
    }

    @Override
    public void addForce(int force) {
        this.electricPower = electricPower + force;
    }

    @Override
    public void removeForce(int force) {
        this.electricPower = electricPower - force;
    }

    @Override
    public int getForce() {
        return electricPower;
    }

    @Override
    public void setForce(int force) {
        this.electricPower = force;
    }

    @Override
    public String toString() {
        return " electricPower: " + electricPower;
    }

}
