package com.geekhub.season4.homework2;

public class DieselEngine implements ForceProvider {

    int horsePower;

    public DieselEngine(int horsepower) {
        this.horsePower = horsepower;
    }

    @Override
    public void addForce(int force) {
        this.horsePower = horsePower + force;
    }

    @Override
    public void removeForce(int force) {
        this.horsePower = horsePower - force;
    }

    @Override
    public int getForce() {
        return horsePower;
    }

    @Override
    public void setForce(int horsePower) {
        this.horsePower = horsePower;
    }

    @Override
    public String toString() {
        return " horsePower: " + horsePower;
    }

}
