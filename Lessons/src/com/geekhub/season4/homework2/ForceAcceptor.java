package com.geekhub.season4.homework2;

public interface ForceAcceptor {

    void turnLeft();

    void turnRight();

    void aligin();

    void addSpeed(int speedVechile);

    void removeSpeed(int speedVechile);

    int getSpeed();

    void setSpeed(int speed);

}
