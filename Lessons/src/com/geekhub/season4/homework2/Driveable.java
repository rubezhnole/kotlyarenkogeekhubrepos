package com.geekhub.season4.homework2;

public interface Driveable {

    void accelerate(int pressThePedal);

    void brake(int pressThePedal);

    void turnLeft(int angleTurnLeft);

    void turnRight(int angleTurnRight);

}
