package com.geekhub.season4.homework2;

public interface EnergyProvider {

    void addEnergy(int energy);

    void removeEnergy(int energy);

    int getEnergy();

    void setEnergy(int energy);

}
