package com.geekhub.season4.homework2;

public class Propeller implements ForceAcceptor {

    public String screw;
    public int speed;

    public Propeller(int speed) {
        this.screw = "|";
        this.speed = speed;
    }

    public void turnLeft() {
        this.screw = "/";
    }

    public void turnRight() {
        this.screw = "\\";
    }

    public void aligin() {
        this.screw = "|";
    }

    @Override
    public void addSpeed(int speedVechile) {
        this.speed = speed + speedVechile;
    }

    @Override
    public void removeSpeed(int speedVechile) {
        this.speed = speed - speedVechile;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    public String toString() {
        return " /\\\n" + "| |\n" + "---\n" + " "
                + this.screw + "\n speed: " + this.speed;
    }

}
