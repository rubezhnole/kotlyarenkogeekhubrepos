package com.geekhub.season4.homework2;

public interface ForceProvider {

    void addForce(int force);

    void removeForce(int force);

    int getForce();

    void setForce(int force);

}
