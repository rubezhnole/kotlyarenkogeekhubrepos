package com.geekhub.season4.homework3.taskThree;

public class TestClass {

    public static void main(String[] args) {

        Car car = new Car(20, 5, 300);         // car

        System.out.println(car.toString());

        try {
            car.accelerate(20);
        } catch (EnergyProviderException e) {
            System.out.println("кончилось топливо, заправлено на 20 ед.");

            car.fillUpEnergy(20);
        }

        System.out.println(car.toString());

        try {
            car.turnRight(14);
        } catch (ForceAcceptorException e) {
            System.out.println("недопустимая скорость давим на педаль, скорость + 20 ед.");

            car.increaseSpeedAt(20);
        } catch (ForceProviderException e) {
            System.out.println("недопустимая мощность давим на педаль, скорость + 20 ед.");

            car.increaseForce(20);
        } catch (EnergyProviderException e) {
            System.out.println("кончилось топливо, заправлено на 20 ед.");

            car.fillUpEnergy(20);
        }

        System.out.println(car.toString());

        try {
            car.turnLeft(16);
        } catch (ForceAcceptorException e) {
            System.out.println("недопустимая скорость давим на педаль, скорость + 20 ед.");

            car.increaseSpeedAt(20);
        } catch (ForceProviderException e) {
            System.out.println("недопустимая мощность давим на педаль, скорость + 20 ед.");

            car.increaseForce(20);
        } catch (EnergyProviderException e) {
            System.out.println("кончилось топливо, заправлено на 20 ед.");

            car.fillUpEnergy(20);
        }

        System.out.println(car.toString());

        try {
            car.brake(20);
        } catch (ForceAcceptorException e) {
            System.out.println("недопустимая скорость давим на педаль, скорость + 20 ед.");

            car.increaseSpeedAt(20);
        } catch (ForceProviderException e) {
            System.out.println("недопустимая мощность давим на педаль, скорость + 20 ед.");

            car.increaseForce(20);
        } catch (EnergyProviderException e) {
            System.out.println("кончилось топливо, заправлено на 20 ед.");

            car.fillUpEnergy(20);
        }

        System.out.println(car.toString());

        Boat boat = new Boat(120, 2, 160);       // boat

        System.out.println(boat.toString());

        try {
            boat.accelerate(45);
        } catch (EnergyProviderException e) {
            System.out.println("кончилось топливо, заправлено на 20 ед.");

            boat.fillUpEnergy(20);
        }

        System.out.println(boat.toString());

        try {
            boat.turnLeft(12);
        } catch (ForceAcceptorException e) {
            System.out.println("недопустимая скорость давим на педаль, скорость + 20 ед.");

            boat.increaseSpeedAt(20);
        } catch (ForceProviderException e) {
            System.out.println("недопустимая мощность давим на педаль, скорость + 20 ед.");

            boat.increaseForce(20);
        } catch (EnergyProviderException e) {
            System.out.println("кончилось топливо, заправлено на 20 ед.");

            boat.fillUpEnergy(20);
        }

        System.out.println(boat.toString());

        try {
            boat.turnRight(45);

            System.out.println(boat.toString());
        } catch (ForceAcceptorException e) {
            System.out.println("недопустимая скорость давим на педаль, скорость + 20 ед.");

            boat.increaseSpeedAt(20);
        } catch (ForceProviderException e) {
            System.out.println("недопустимая мощность давим на педаль, скорость + 20 ед.");

            boat.increaseForce(20);
        } catch (EnergyProviderException e) {
            System.out.println("кончилось топливо, заправлено на 20 ед.");

            boat.fillUpEnergy(20);
        }

        SolarPoweredCar solarCar = new SolarPoweredCar(200, 100, 250);        // solar car

        System.out.println(solarCar.toString());

        try {
            solarCar.accelerate(20);
        } catch (EnergyProviderException e) {
            System.out.println("кончилось топливо, заправлено на 20 ед.");

            solarCar.fillUpEnergy(20);
        }

        System.out.println(solarCar.toString());

        try {
            solarCar.turnLeft(14);
        } catch (ForceAcceptorException e) {
            System.out.println("недопустимая скорость давим на педаль, скорость + 20 ед.");

            solarCar.increaseSpeedAt(20);
        } catch (ForceProviderException e) {
            System.out.println("недопустимая мощность давим на педаль, скорость + 20 ед.");

            solarCar.increaseForce(20);
        } catch (EnergyProviderException e) {
            System.out.println("кончилось топливо, заправлено на 20 ед.");

            solarCar.fillUpEnergy(20);
        }

        System.out.println(solarCar.toString());

        try {
            solarCar.brake(15);
        } catch (ForceAcceptorException e) {
            System.out.println("недопустимая скорость давим на педаль, скорость + 20 ед.");

            solarCar.increaseSpeedAt(20);
        } catch (ForceProviderException e) {
            System.out.println("недопустимая мощность давим на педаль, скорость + 20 ед.");

            solarCar.increaseForce(20);
        } catch (EnergyProviderException e) {
            System.out.println("кончилось топливо, заправлено на 20 ед.");

            solarCar.fillUpEnergy(20);
        }

        System.out.println(solarCar.toString());

        try {
            solarCar.turnRight(23);
        } catch (ForceAcceptorException e) {
            System.out.println("недопустимая скорость давим на педаль, скорость + 20 ед.");

            solarCar.increaseSpeedAt(20);
        } catch (ForceProviderException e) {
            System.out.println("недопустимая мощность давим на педаль, скорость + 20 ед.");

            solarCar.increaseForce(20);
        } catch (EnergyProviderException e) {
            System.out.println("кончилось топливо, заправлено на 20 ед.");

            solarCar.fillUpEnergy(20);
        }

        System.out.println(solarCar.toString());

        try {
            solarCar.accelerate(14);
        } catch (EnergyProviderException e) {
            System.out.println("кончилось топливо, заправлено на 20 ед.");

            solarCar.fillUpEnergy(20);
        }

        System.out.println(solarCar.toString());

    }
}
