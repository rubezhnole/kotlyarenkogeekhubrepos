package com.geekhub.season4.homework3.taskThree;

public abstract class Vechile implements Driveable {

    private ForceProvider forceProvider;
    private EnergyProvider energyProvider;
    private ForceAcceptor forceAcceptor;

    public Vechile(ForceProvider forceProvider, EnergyProvider energyProvider, ForceAcceptor forceAcceptor) {
        this.forceProvider = forceProvider;
        this.energyProvider = energyProvider;
        this.forceAcceptor = forceAcceptor;
    }

    @Override
    public void accelerate(int pressThePedal) throws EnergyProviderException {
        forceAcceptor.aligin();
        forceAcceptor.addSpeed((energyProvider.getEnergy() / 2) / pressThePedal);
        forceProvider.addForce(forceAcceptor.getSpeed() / pressThePedal);
        energyProvider.removeEnergy(forceAcceptor.getSpeed() / forceProvider.getForce());
    }

    @Override
    public void brake(int pressThePedal) throws ForceAcceptorException,
                                                ForceProviderException,
                                                EnergyProviderException {
        forceAcceptor.aligin();
        forceAcceptor.removeSpeed((energyProvider.getEnergy() / 2) / pressThePedal);
        forceProvider.removeForce(forceAcceptor.getSpeed() / pressThePedal);
        energyProvider.removeEnergy(forceAcceptor.getSpeed() / (forceProvider.getForce() / 2));
    }

    @Override
    public void turnLeft(int angleTurnLeft) throws ForceAcceptorException,
                                                   ForceProviderException,
                                                   EnergyProviderException {
        forceAcceptor.removeSpeed((energyProvider.getEnergy() / 4) / angleTurnLeft);
        forceProvider.removeForce(forceAcceptor.getSpeed() / angleTurnLeft);
        energyProvider.removeEnergy(forceAcceptor.getSpeed() / (forceProvider.getForce() / 4));
        forceAcceptor.turnLeft();
    }

    @Override
    public void turnRight(int angleTurnRight) throws ForceAcceptorException,
                                                     ForceProviderException,
                                                     EnergyProviderException {
        forceAcceptor.removeSpeed((energyProvider.getEnergy() / 4) / angleTurnRight);
        forceProvider.removeForce(forceAcceptor.getSpeed() / angleTurnRight);
        energyProvider.removeEnergy(forceAcceptor.getSpeed() / (forceProvider.getForce() / 4));
        forceAcceptor.turnRight();
    }

    @Override
    public void fillUpEnergy(int energy) {
        energyProvider.setEnergy(0);
        energyProvider.addEnergy(energy);
    }

    @Override
    public void increaseSpeedAt(int speed) {
        forceAcceptor.setSpeed(0);
        forceAcceptor.addSpeed(speed);
    }

    @Override
    public void showFuel() {
        System.out.println("топлива осталось: " + energyProvider.getEnergy());
    }

    @Override
    public void increaseForce(int force) {
        forceProvider.setForce(0);
        forceProvider.addForce(force);
    }

    @Override
    public String toString() {
        return forceAcceptor.toString() + forceProvider.toString() + energyProvider.toString();
    }

}
