package com.geekhub.season4.homework3.taskThree;

public class ForceProviderException extends Exception {

    public ForceProviderException() {
    }

    public ForceProviderException(String message) {
        super(message);
    }
}
