package com.geekhub.season4.homework3.taskThree;

public class Wheel implements ForceAcceptor {

    public String wheels[];
    public int speed;                                   // скорость автомобиля

    public Wheel(int speed) {
        this.wheels = new String[]{"|", "|", "|", "|"}; // иллюстрация положения колес автомобиля
        this.speed = speed;
    }

    @Override
    public void addSpeed(int speedVechile) {
        this.speed = speed + speedVechile;
    }

    @Override
    public void removeSpeed(int speedVechile) throws ForceAcceptorException {

        if (this.speed < 0) {
            throw new ForceAcceptorException("не допустимая скорость для автомобиля.");
        }

        this.speed = this.speed - speedVechile;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void turnLeft() {
        this.wheels[0] = "\\";
        this.wheels[1] = "\\";
    }

    public void turnRight() {
        this.wheels[0] = "/";
        this.wheels[1] = "/";
    }

    public void aligin() {
        this.wheels[0] = "|";
        this.wheels[1] = "|";
    }

    public String toString() {
        return "\n" + this.wheels[0] + " " + this.wheels[1]
                + "\n" + this.wheels[2] + " " + this.wheels[3]
                + "\nspeed: " + this.speed;
    }

}
