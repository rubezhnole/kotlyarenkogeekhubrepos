package com.geekhub.season4.homework3.taskThree;

public interface Driveable {

    void accelerate(int pressThePedal) throws EnergyProviderException, ForceProviderException, ForceAcceptorException;

    void brake(int pressThePedal) throws EnergyProviderException, ForceProviderException, ForceAcceptorException;

    void turnLeft(int angleTurnLeft) throws EnergyProviderException, ForceProviderException, ForceAcceptorException;

    void turnRight(int angleTurnRight) throws EnergyProviderException, ForceProviderException, ForceAcceptorException;

    void fillUpEnergy(int energy);

    void increaseSpeedAt(int speed);

    void showFuel();

    void increaseForce(int force);
}
