package com.geekhub.season4.homework3.taskThree;

public class ElectricEngine implements ForceProvider {

    int electricPower;

    public ElectricEngine(int power) {
        this.electricPower = power;
    }

    @Override
    public void addForce(int force) {
        this.electricPower = electricPower + force;
    }

    @Override
    public void removeForce(int force) throws ForceProviderException {

        if (this.electricPower < 0) {
            throw new ForceProviderException("не допустимая мощность электро-двигателя.");
        }
        this.electricPower = electricPower - force;
    }

    @Override
    public int getForce() {
        return electricPower;
    }

    @Override
    public void setForce(int force) {
        this.electricPower = force;
    }

    @Override
    public String toString() {
        return " electricPower: " + electricPower;
    }

}
