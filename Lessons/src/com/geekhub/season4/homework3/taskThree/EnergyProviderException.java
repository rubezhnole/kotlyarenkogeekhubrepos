package com.geekhub.season4.homework3.taskThree;

public class EnergyProviderException extends Exception {

    public EnergyProviderException() {
    }

    public EnergyProviderException(String message) {
        super(message);
    }
}
