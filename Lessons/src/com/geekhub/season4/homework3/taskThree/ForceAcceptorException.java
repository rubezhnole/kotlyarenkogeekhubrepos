package com.geekhub.season4.homework3.taskThree;

public class ForceAcceptorException extends Exception {

    public ForceAcceptorException() {
    }

    public ForceAcceptorException(String message) {
        super(message);
    }

}
