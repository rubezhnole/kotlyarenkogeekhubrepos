package com.geekhub.season4.homework3.taskThree;

public interface ForceProvider {

    void addForce(int force);

    void removeForce(int force) throws ForceProviderException;

    int getForce();

    void setForce(int force);

}
