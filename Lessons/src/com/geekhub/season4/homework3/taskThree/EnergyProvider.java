package com.geekhub.season4.homework3.taskThree;

public interface EnergyProvider {

    void addEnergy(int energy);

    void removeEnergy(int energy) throws  EnergyProviderException;

    int getEnergy();

    void setEnergy(int energy);

}
