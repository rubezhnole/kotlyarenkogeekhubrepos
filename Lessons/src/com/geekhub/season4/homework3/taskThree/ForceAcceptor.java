package com.geekhub.season4.homework3.taskThree;

public interface ForceAcceptor {

    void turnLeft();

    void turnRight();

    void aligin();

    void addSpeed(int speedVechile);

    void removeSpeed(int speedVechile) throws ForceAcceptorException;

    int getSpeed();

    void setSpeed(int speed);

}
