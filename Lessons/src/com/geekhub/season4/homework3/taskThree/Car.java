package com.geekhub.season4.homework3.taskThree;

public class Car extends Vechile {

    Chassis chassis;

    public Car(int force, int energy, int speed) {
        super(new DieselEngine(force), new GasTank(energy), new Wheel(speed));

        chassis = new Chassis();
    }

    @Override
    public String toString() {
        return super.toString() + chassis.toString();
    }

}
