package com.geekhub.season4.homework3.taskThree;

public class GasTank implements EnergyProvider {

    int fuel;

    public GasTank(int gas) {
        this.fuel = gas;
    }

    @Override
    public void addEnergy(int energy) {
        this.fuel = fuel + energy;
    }

    @Override
    public void removeEnergy(int energy) throws EnergyProviderException{

        if (this.fuel < 0) {
            throw new EnergyProviderException("не допустимое количество топлива.");
        }

        this.fuel = fuel - energy;
    }

    @Override
    public int getEnergy() {
        return fuel;
    }

    @Override
    public void setEnergy(int energy) {
        this.fuel = energy;
    }

    @Override
    public String toString() {
        return " |fuel: " + fuel;
    }

}

