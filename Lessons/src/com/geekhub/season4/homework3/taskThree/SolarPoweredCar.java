package com.geekhub.season4.homework3.taskThree;

public class SolarPoweredCar extends Vechile {

    Chassis chassis;

    public SolarPoweredCar(int force, int energy, int speed) {
        super(new ElectricEngine(force), new SolarBattery(energy), new Wheel(speed));

        chassis = new Chassis();
    }

    @Override
    public String toString() {
        return super.toString() + chassis.toString();
    }

}
