package com.geekhub.season4.homework3.taskThree;

public class SolarBattery implements EnergyProvider {

    int battery;

    public SolarBattery(int bat) {
        this.battery = bat;
    }

    @Override
    public void addEnergy(int energy) {
        this.battery = battery + energy;
    }

    @Override
    public void removeEnergy(int energy) throws EnergyProviderException {

        if (this.battery < 0) {
            throw new EnergyProviderException("не допустимый заряд.");
        }

        this.battery = battery - energy;
    }

    @Override
    public int getEnergy() {
        return battery;
    }

    @Override
    public void setEnergy(int energy) {
        this.battery = energy;
    }

    @Override
    public String toString() {
        return " |solar battery: " + battery;
    }

}
