package com.geekhub.season4.homework3.taskThree;

public class DieselEngine implements ForceProvider {

    int horsePower;

    public DieselEngine(int horsepower) {
        this.horsePower = horsepower;
    }

    @Override
    public void addForce(int force) {
        this.horsePower = horsePower + force;
    }

    @Override
    public void removeForce(int force) throws ForceProviderException {

        if (this.horsePower < 0) {
            throw new ForceProviderException("не допустимая мощность двигателя.");
        }

        this.horsePower = horsePower - force;
    }

    @Override
    public int getForce() {
        return horsePower;
    }

    @Override
    public void setForce(int horsePower) {
        this.horsePower = horsePower;
    }

    @Override
    public String toString() {
        return " horsePower: " + horsePower;
    }

}
