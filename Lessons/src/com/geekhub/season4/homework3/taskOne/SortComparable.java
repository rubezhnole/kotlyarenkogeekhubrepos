package com.geekhub.season4.homework3.taskOne;

public class SortComparable {

    public static Comparable[] sort(Comparable[] elements) {
        Comparable[] tmpArr = elements.clone();

        for (int i = 0; i < tmpArr.length; i++) {
            for (int j = tmpArr.length - 1; j > i; j--) {

                if (tmpArr[j].compareTo(tmpArr[j - 1]) < 0) {
                    Comparable temp = tmpArr[j];
                    tmpArr[j] = tmpArr[j - 1];
                    tmpArr[j - 1] = temp;
                }
            }
        }

        return tmpArr;
    }

    public static Comparable[] qSort(Comparable[] elements) {
        Comparable[] tmpArr = elements.clone();

        quickSort(tmpArr);

        return tmpArr;
    }

    public static void quickSort(Comparable[] elements) {
        recursiveQuickSort(elements, 0, elements.length - 1);
    }

    public static void recursiveQuickSort(Comparable[] array, int startIdx, int endIdx) {
        int idx = partition(array, startIdx, endIdx);

        if (startIdx < idx - 1) {
            recursiveQuickSort(array, startIdx, idx - 1);
        }

        if (endIdx > idx) {
            recursiveQuickSort(array, idx, endIdx);
        }
    }

    public static int partition(Comparable[] array, int left, int right) {
        Comparable pivot = array[left];

        while (left <= right) {

            while (array[left].compareTo(pivot) < 0) {     
                left++;
            }

            while (array[right].compareTo(pivot) > 0) {   
                right--;
            }

            if (left <= right) {
                Comparable tmp = array[left];
                array[left] = array[right];
                array[right] = tmp;

                left++;
                right--;
            }
        }

        return left;
    }

    public static void main(String[] args) {

        Car car[] = new Car[5];
        Car rezCar[];

        car[0] = new Car("Ferrari");
        car[1] = new Car("Bently");
        car[2] = new Car("Lamborgini");
        car[3] = new Car("Ford");
        car[4] = new Car("Audi");

        System.out.println("=========== исходный массив объектов Car.");

        for (Car c : car) {
            System.out.println(c.toString());
        }

        rezCar = (Car[]) sort(car);

        System.out.println("=========== сортированный массив объектов Car (bubble sort).");

        for (Car c : rezCar) {
            System.out.println(c.toString());
        }

        rezCar = (Car[]) qSort(car);

        System.out.println("=========== сортированный массив объектов Car (quick sort).");

        for (Car c : rezCar) {
            System.out.println(c.toString());
        }

        System.out.println("=========== исходный массив объектов Car остался без изменений.");

        for (Car c : car) {
            System.out.println(c.toString());
        }

        System.out.println("=========== исходный массив объектов String.");

        String stringBegin[] = new String[8];
        String stringEnd[];

        stringBegin[0] = "d";
        stringBegin[1] = "c";
        stringBegin[2] = "r";
        stringBegin[3] = "i";
        stringBegin[4] = "k";
        stringBegin[5] = "o";
        stringBegin[6] = "b";
        stringBegin[7] = "m";

        for (String str : stringBegin) {
            System.out.println(str);
        }

        System.out.println("=========== сортированный массив объектов String (bubble sort).");

        stringEnd = (String[]) sort(stringBegin);

        for (String str : stringEnd) {
            System.out.println(str);
        }

        System.out.println("=========== сортированный массив объектов String (quick sort).");

        stringEnd = (String[]) qSort(stringBegin);

        for (String str : stringEnd) {
            System.out.println(str);
        }

        System.out.println("=========== исходный массив объектов String остался без изменений.");

        for (String str : stringBegin) {
            System.out.println(str);
        }

        Integer integerArrayBegin[] = new Integer[100];
        Integer integerArrayEnd[];

        for (int i = 0; i < integerArrayBegin.length; i++) {
            integerArrayBegin[i] = (int) Math.round(Math.random() * 100);
        }

        System.out.println("=========== исходный массив объектов Integer.");

        for (Integer tmp : integerArrayBegin) {
            System.out.println(tmp.toString());
        }

        System.out.println("=========== сортированный массив объектов Integer (bubble sort).");

        integerArrayEnd = (Integer[]) sort(integerArrayBegin);

        for (Integer tmp : integerArrayEnd) {
            System.out.println(tmp.toString());
        }

        System.out.println("=========== сортированный массив объектов Integer (quick sort).");

        integerArrayEnd = (Integer[]) qSort(integerArrayBegin);

        for (Integer tmp : integerArrayEnd) {
            System.out.println(tmp.toString());
        }

        System.out.println("=========== исходный массив объектов Integer остался без изменений.");

        for (Integer tmp : integerArrayBegin) {
            System.out.println(tmp.toString());
        }
    }
}
