package com.geekhub.season4.homework3.taskOne;

public class Car implements Comparable {

    public String carBrand;

    public Car(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    @Override
    public int compareTo(Object o) {
        Car tmpCar = (Car) o;

        return this.getCarBrand().compareTo(tmpCar.getCarBrand());
    }

    @Override
    public String toString() {
        return "car brand: " + getCarBrand();
    }
}
