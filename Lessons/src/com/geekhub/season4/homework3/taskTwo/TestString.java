package com.geekhub.season4.homework3.taskTwo;

public class TestString {

    public static void main(String[] args) {

        Long timeMillisB;
        Long timeMillisE;

        String testString = "0";
        StringBuffer stringBufferTest;
        StringBuilder stringBuilderTest;

        stringBufferTest = new StringBuffer("0");
        stringBuilderTest = new StringBuilder("0");


        timeMillisB = System.currentTimeMillis();                                // String test

        for (int i = 1; i <= 15000; i++) {
            testString = testString + i;
        }

        timeMillisE = System.currentTimeMillis();

        System.out.println("String millis: " + (timeMillisE - timeMillisB));

        timeMillisB = System.currentTimeMillis();                                 // StringBuffer test

        for (int i = 1; i <= 15000; i++) {
            stringBufferTest.append(i);
        }

        timeMillisE = System.currentTimeMillis();

        System.out.println("StringBuffer millis: " + (timeMillisE - timeMillisB));


        timeMillisB = System.currentTimeMillis();                                  // StringBuilder test

        for (int i = 1; i <= 15000; i++) {
            stringBuilderTest.append(i);
        }

        timeMillisE = System.currentTimeMillis();

        System.out.println("StringBuilder millis: " + (timeMillisE - timeMillisB));
    }
}
