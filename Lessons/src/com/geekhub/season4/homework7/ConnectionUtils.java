package com.geekhub.season4.homework7;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Utils class that contains useful method to interact with URLConnection
 */
public class ConnectionUtils {

    /**
     * Downloads content for specified URL and returns it as a byte array.
     * Should be used for small files only. Don't use it to download big files it's dangerous.
     * @param url
     * @return
     * @throws IOException
     */
    public static byte[] getData(URL url) throws IOException {

        URLConnection connection = url.openConnection();
        int c;
        StringBuilder sb = new StringBuilder();

        try (InputStream is = connection.getInputStream()) {

            while ((c = is.read()) != -1) {
                sb.append((char) c);
            }

            return sb.toString().getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}