package com.geekhub.season4.homework8;


import com.geekhub.season4.homework8.objects.Cat;
import com.geekhub.season4.homework8.objects.User;
import com.geekhub.season4.homework8.storage.DatabaseStorage;
import com.geekhub.season4.homework8.storage.Storage;

import java.sql.*;
import java.util.List;

public class Test {
    public static void main(String[] args) throws Exception {
        Connection connection = createConnection("root", "root", "jdbc:mysql://localhost:3307/geekdb");
        Storage storage = new DatabaseStorage(connection);
        List<Cat> cats = storage.list(Cat.class);

        for (Cat cat : cats) {
            System.out.println(storage.delete(cat));
        }

        cats = storage.list(Cat.class);
        try {
            if (!cats.isEmpty()) {
                throw new Exception("Cats should not be in database!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        for(int i = 1; i <= 20; i++) {
            Cat cat = new Cat();
            cat.setName("cat" + i);
            cat.setAge(i);
            storage.save(cat);
        }

        cats = storage.list(Cat.class);
        try {
            if (cats.size() != 20) {
                new Exception("Number of cats in storage should be 20!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        User user = new User();
        user.setAdmin(true);
        user.setAge(23);
        user.setName("Victor");
        user.setBalance(22.23);
        storage.save(user);

        User user1 = storage.get(User.class, user.getId());
        if (!user1.getName().equals(user.getName())) throw new Exception("Users should be equals!");

        user.setAdmin(false);
        storage.save(user);

        User user2 = storage.get(User.class, user.getId());
        if (!user.getAdmin().equals(user2.getAdmin())) throw new Exception("Users should be updated!");

        storage.delete(user1);

        User user3 = storage.get(User.class, user.getId());

        if (user3 != null) throw new Exception("User should be deleted!");

        connection.close();
        System.out.println("Disconnection...");
    }

    private static Connection createConnection(String login, String password, String dbName) throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection connection = DriverManager.getConnection(dbName, login, password);
        System.out.println("Create connection...");

        return connection;
    }
}
