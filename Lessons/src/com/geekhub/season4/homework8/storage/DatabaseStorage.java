package com.geekhub.season4.homework8.storage;

import com.geekhub.season4.homework8.objects.Entity;
import com.geekhub.season4.homework8.objects.Ignore;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;

/**
 * Implementation of {@link com.geekhub.season4.homework8.storage.Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link com.geekhub.season4.homework8.objects.Entity} class.
 * Could be created only with {@link java.sql.Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {

        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM "
                + clazz.getSimpleName() + " WHERE id = ?")) {
            preparedStatement.setInt(1, id);
            List<T> result = extractResult(clazz, preparedStatement.executeQuery());

            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {

        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM "
                + clazz.getSimpleName())) {
            List<T> result = extractResult(clazz, preparedStatement.executeQuery());

            return result.isEmpty() ? null : result;
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {

        try (PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM "
                + entity.getClass().getSimpleName() + " WHERE id = ?")) {
            preparedStatement.setInt(1, entity.getId());

            return preparedStatement.executeUpdate() > 0;
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);

        String fields = "";
        String value = "";

        for (Map.Entry<String, Object> tmp : data.entrySet()) {
            fields = fields + tmp.getKey() + ",";

            if (isString(tmp.getValue())) {
                value = value + "'" + tmp.getValue() + "',";
            } else {
                value = value + tmp.getValue() + ",";
            }
        }

        fields = fields.substring(0, fields.length() - 1);
        value = value.substring(0, value.length() - 1);

        if (entity.isNew()) {
            String sqlInsert = "INSERT INTO " + entity.getClass().getSimpleName().toLowerCase()
                    + " (" + fields + ") VALUES (" + value + ")";

            System.out.println("sql : " + sqlInsert);

            try (PreparedStatement pS = connection.prepareStatement(sqlInsert, Statement.RETURN_GENERATED_KEYS)) {
                pS.executeUpdate();

                ResultSet keys = pS.getGeneratedKeys();

                if (keys != null && keys.next()) {
                    entity.setId(keys.getInt(1));
                }
            }
        } else {
            String set = "";

            for (Map.Entry<String, Object> tmp : data.entrySet()) {

                if (isString(tmp.getValue())) {
                    set = set + tmp.getKey() + " = '" + tmp.getValue() + "',";
                } else {
                    set = set + tmp.getKey() + " = " + tmp.getValue() + ",";
                }
            }

            set = set.substring(0, set.length() - 1);
            String sqlUpdate = "UPDATE " + entity.getClass().getSimpleName().toLowerCase()
                    + " SET " + set + " WHERE id = " + entity.getId();

            System.out.println("set : " + set);
            System.out.println(sqlUpdate);

            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate(sqlUpdate);
            }
        }
    }

    private boolean isString(Object o) {
        String typeName = o.getClass().getTypeName();

        return typeName.equals("java.lang.String");
    }

    //converts object to map, could be helpful in save method
    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        Map<String, Object> result = new HashMap<>();

        for (Field f : entity.getClass().getDeclaredFields()) {
            f.setAccessible(true);

            if (f.isAnnotationPresent(Ignore.class)) {
                System.out.println(f.getName() + " was ignored!");
                continue;
            }

            System.out.println(f.getName() + " = " + f.get(entity));

            result.put(f.getName(), f.get(entity));
            f.setAccessible(false);
        }

        return result;
    }

    //creates list of new instances of clazz by using data from resultset
    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultset) throws Exception {
        List<T> result = new ArrayList<>();
        Field[] declaredFields = clazz.getDeclaredFields();
        int i = 0;

        while (resultset.next()) {
            result.add(clazz.newInstance());
            result.get(i).setId(resultset.getInt("id"));

            for (Field field : declaredFields) {
                if (field.getAnnotation(Ignore.class) != null) {
                    continue;
                }

                field.setAccessible(true);
                field.set(result.get(i), resultset.getObject(field.getName()));
                field.setAccessible(false);
            }
            i++;
        }

        return result;
    }
}