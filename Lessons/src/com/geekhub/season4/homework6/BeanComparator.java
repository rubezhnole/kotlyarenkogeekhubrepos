package com.geekhub.season4.homework6;

import java.lang.reflect.Field;

public class BeanComparator {

    public boolean beanCompare(Object o1, Object o2) throws IllegalAccessException {
        Class<?> obj1 = o1.getClass();
        Class<?> obj2 = o2.getClass();
        boolean state = true;

        Field[] objField1 = obj1.getDeclaredFields();
        Field[] objField2 = obj2.getDeclaredFields();

        Field.setAccessible(objField1, true);
        Field.setAccessible(objField2, true);

        for (int i = 0; i < objField1.length; i++) {
            if (!objField1[i].isAnnotationPresent(NotComparator.class)) {

                if (isPrimitive(objField1[i])) {
                    if (!objField1[i].get(o1).equals(objField2[i].get(o2))) {
                        System.out.println(objField1[i].get(o1) + " " + objField1[i].getType());
                        System.out.println(objField2[i].get(o2) + " " + objField2[i].getType());

                        return false;
                    }
                } else {
                    state = beanCompare(objField1[i].get(o1), objField2[i].get(o2));
                }
            }
        }
        Field.setAccessible(objField1, false);
        Field.setAccessible(objField2, false);

        return state;
    }

    private boolean isPrimitive(Field field) {
        Class<?> type = field.getType();

        return type.isPrimitive() || type == String.class;
    }
}