package com.geekhub.season4.homework6;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class TestExample {

    public static void main(String[] args) {

        Car car = new Car("red", 200, "sedan", "T supra");
        Class c = car.getClass();

        int mods = c.getModifiers();

        if (Modifier.isPublic(mods)) {
            System.out.println("public");
        }
        if (Modifier.isAbstract(mods)) {
            System.out.println("abstract");
        }
        if (Modifier.isFinal(mods)) {
            System.out.println("final");
        }

        Class superClass = c.getSuperclass();
        System.out.println(superClass.getSimpleName());

        Class[] interfaces = c.getInterfaces();
        for (Class i : interfaces) {
            System.out.println(i.getName());
        }

        Field[] publicField = c.getDeclaredFields();
        for (Field f : publicField) {
            Class fieldType = f.getType();
            try {
                System.out.println("Name: " + f.getName() + ", value: " + f.get(car));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            System.out.println("Type: " + fieldType.getSimpleName());
        }

        /*try {
            Field field = c.getField("color");
            String colorValue = (String) field.get(car);

        } catch (Exception e) {
            e.printStackTrace();
        }*/

        Method[] methods = car.getClass().getMethods();

        for (Method m : methods) {
            System.out.println("Name: " + m.getName());
            System.out.println("Return type: " + m.getReturnType().getName());

            Class[] paramTypes = m.getParameterTypes();
            System.out.println("Parameters type: ");

            for (Class pT : paramTypes)  {
                System.out.println(" " + pT.getName());
            }

            System.out.println();
        }




    }
}
