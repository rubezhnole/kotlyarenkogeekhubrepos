package com.geekhub.season4.homework6;

public class MainTestClass {

    public static void main(String[] args) throws Exception {
        Cat cat = new Cat("Red", 5, 10, 15, new Car("Yellow", 200, "sedan", "RX-7"));
        Cat cat2 = new Cat("Red", 5, 10, 15, new Car("Yellow", 200, "sedan", "RX-8"));
        Car car = new Car("Yellow", 200, "sedan", "RX-8");
        Human human = new Human(180, "Male", 23, 80);

        BeanRepresenter beanRepresenter = new BeanRepresenter();
        CloneCreator cloneCreator = new CloneCreator();
        BeanComparator beanComparator = new BeanComparator();

        beanRepresenter.representObject(cat);
        System.out.println("====================");
        beanRepresenter.representObject(car);
        System.out.println("====================");
        beanRepresenter.representObject(human);
        System.out.println("====================");
        beanRepresenter.representObject(cat);
        System.out.println("====================");

        Car clone = cloneCreator.createClone(car);
        beanRepresenter.representObject(clone);

        System.out.println("||||||||||||||||||||");

        Human h1 = new Human(180, "Male", 18, 58);
        Human h2 = new Human(180, "Male", 18, 58);

        System.out.println(beanComparator.beanCompare(h1, h2));
        System.out.println(beanComparator.beanCompare(cat, cat2));
    }
}
