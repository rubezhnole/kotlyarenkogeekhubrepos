package com.geekhub.season4.homework6;

public class Human {

    int height;
    String gender;
    int age;
    int weight;

    public Human(int height, String gender, int age, int weight) {
        this.height = height;
        this.gender = gender;
        this.age = age;
        this.weight = weight;
    }
}
