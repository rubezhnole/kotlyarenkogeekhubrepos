package com.geekhub.season4.homework6;

import java.lang.reflect.Field;

public class BeanRepresenter {

    public void representObject(Object o) {

        if (o != null) {
            Class<?> aClass = o.getClass();

            System.out.println("class " + aClass.getSimpleName());

            for (Field f : aClass.getDeclaredFields()) {
                f.setAccessible(true);

                if (isPrimitive(f)) {
                    try {
                        System.out.println(f.getName() + " " + f.get(o));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {

                        System.out.println("= " + f.getName());
                        representObject(f.get(o));
                        System.out.println("==");
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
                f.setAccessible(false);
            }
        } else {
            System.out.println("Attention! Object not initialized...");
        }
    }

    private boolean isPrimitive(Field field) {
        Class<?> type = field.getType();

        return type.isPrimitive() || type == String.class;
    }
}