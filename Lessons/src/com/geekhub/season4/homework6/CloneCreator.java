package com.geekhub.season4.homework6;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class CloneCreator {

    private final static Map<Class<?>, Object> defaultValues = new HashMap<>();

    static {
        defaultValues.put(int.class, 0);
        defaultValues.put(long.class, 0L);
        defaultValues.put(byte.class, 0);
        defaultValues.put(short.class, 0);
        defaultValues.put(double.class, 0d);
        defaultValues.put(char.class, '\0');
        defaultValues.put(float.class, 0f);
    }

    public <T> T createClone(T o) throws InstantiationException, IllegalAccessException, InvocationTargetException {
        Class<?> oClass = o.getClass();

        Constructor<?>[] constructors = oClass.getConstructors();
        Constructor<?> constructor = constructors[0];

        int parameterCount = constructor.getParameterCount();
        Object[] emptyObjects = emptyObject(parameterCount, constructor.getParameterTypes());
        T instance = (T) constructor.newInstance(emptyObjects);

        Field[] declaredFields = oClass.getDeclaredFields();
        Field.setAccessible(declaredFields, true);

        for (Field field : declaredFields) {
            Object fieldValue = isPrimitive(field) ? field.get(o) : createClone(field.get(o));
            field.set(instance, fieldValue);
        }

        return instance;
    }

    private Object[] emptyObject(int parameterCount, Class<?>[] parameterType)
            throws InstantiationException, IllegalAccessException {
        Object[] resultObg = new Object[parameterCount];

        for (int i = 0; i < resultObg.length; i++) {
            Class<?> type = parameterType[i];

            if (type.isPrimitive()) {
                resultObg[i] = defaultValues.get(type);
            } else {
                resultObg[i] = type.newInstance();
            }
        }

        return resultObg;
    }

    private boolean isPrimitive(Field field) {
        Class<?> type = field.getType();

        return type.isPrimitive() || type == String.class;
    }
}