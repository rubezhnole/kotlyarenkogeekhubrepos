package com.geekhub.season4.homework6;

public class Cat {

    String color;
    int age;
    int legCount;
    int furLength;
    Car catCar;

    public Cat(String color, int age, int legCount, int furLength, Car car) {
        this.color = color;
        this.age = age;
        this.legCount = legCount;
        this.furLength = furLength;
        this.catCar = car;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "color='" + color + '\'' +
                ", age=" + age +
                ", legCount=" + legCount +
                ", furLength=" + furLength +
                '}' + "car " + catCar.toString();
    }
}
