package com.geekhub.season4.homework4.task2;

import java.util.*;
import java.util.function.Predicate;

public class SortedTaskManager implements TaskManager {
    private Map<Date, Task> registry = new TreeMap<>();

    @Override
    public void addTask(Date date, Task task) {
        registry.put(date, task);
    }

    @Override
    public void removeTask(Date date) {
        registry.remove(date);
    }

    @Override
    public Set<String> getCategories() {
        Set<String> result = new HashSet<String>();
        Set<Task> tasks = new HashSet<>(registry.values());

        tasks.stream()
                .forEach((task) -> result.add(task.getCategory())
                );

        return result;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        Map<String, List<Task>> map = new HashMap<>();

        for (Date date : registry.keySet()) {
            Task task = registry.get(date);

            String category = task.getCategory();

            if (map.get(category) == null) {
                map.put(category, new ArrayList<Task>());
            }

            map.get(category).add(task);
        }

        return map;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        List<Task> resList = new ArrayList<>();
        Set<Task> taskList = new HashSet<>(registry.values());

        taskList.stream()
                .filter((t) -> t.getCategory().equals(category))
                .forEach(resList::add);

        return resList;
    }

    @Override
    public List<Task> getTasksForToday() {
        Calendar today = Calendar.getInstance();
        List<Task> result = new ArrayList<>();
        Set<Date> date = registry.keySet();

        date.stream()
                .filter((d) -> {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(d);

                    boolean state = calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR)
                            && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR);
                    return state;
                })
                .forEach((d) -> result.add(registry.get(d)));

        return result;
    }


    @Override
    public List<Task> filterTasks(Predicate<Task> predicate) {
        List<Task> res = new ArrayList<>();
        Set<Task> task = new HashSet<>(registry.values());

        task.stream()
                .filter(predicate::test)
                .forEach(res::add);

        return res;
    }
}
