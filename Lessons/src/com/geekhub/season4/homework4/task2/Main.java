package com.geekhub.season4.homework4.task2;


import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        TaskManager taskManager = new SortedTaskManager();

        Calendar cal = Calendar.getInstance();
        cal.set(2014, Calendar.NOVEMBER, 29, 7, 30);
        taskManager.addTask(cal.getTime(), new Task("sport", "Morning exercises"));

        cal.set(Calendar.HOUR, 8);
        cal.set(Calendar.MINUTE, 0);
        taskManager.addTask(cal.getTime(), new Task("misc", "Shower"));

        cal.set(Calendar.HOUR, 8);
        cal.set(Calendar.MINUTE, 20);
        taskManager.addTask(cal.getTime(), new Task("misc", "Eating"));

        cal.set(Calendar.HOUR, 9);
        cal.set(Calendar.MINUTE, 0);
        taskManager.addTask(cal.getTime(), new Task("work", "Team meeting"));

        cal.set(Calendar.HOUR, 9);
        cal.set(Calendar.MINUTE, 30);
        taskManager.addTask(cal.getTime(), new Task("work", "Working"));

        cal.set(Calendar.HOUR, 20);
        cal.set(Calendar.MINUTE, 30);
        taskManager.addTask(cal.getTime(), new Task("education", "Scala programming"));

        cal.set(Calendar.HOUR, 18);
        cal.set(Calendar.MINUTE, 30);
        taskManager.addTask(cal.getTime(), new Task("sport", "Gym"));



        /*System.out.println("Categories so far: " + taskManager.getCategories());

        System.out.println("Task for today: ");
        for (Task task : taskManager.getTasksForToday()) {
            System.out.println(task.getDescription());
        }

        Map<String, List<Task>> taskByCategories = taskManager.getTasksByCategories();
        for (String category : taskByCategories.keySet()) {
            System.out.println(category);

            for (Task task : taskByCategories.get(category)) {
                System.out.println("    " + task.getDescription());
            }
        }*/

        System.out.println("Task by category: ");

        Map<String, List<Task>> taskByCategories = taskManager.getTasksByCategories();

        for (String category : taskByCategories.keySet()) {
            System.out.println(category);

            for (Task task : taskByCategories.get(category)) {
                System.out.println("    " + task.getDescription());
            }
        }

        System.out.println("Category :");

        Set<String> categories = taskManager.getCategories();

        for (String s : categories) {
            System.out.println(s);
        }

        System.out.println("Task by category: ");

        List<Task> taskByCat = taskManager.getTasksByCategory("work");

        for (Task task : taskByCat) {
            System.out.println(task.toString());
        }

        System.out.println("Task fo today: ");

        List<Task> taskForToday = taskManager.getTasksForToday();

        for (Task t : taskForToday) {
            System.out.println(t.toString());
        }


    }
}
