package com.geekhub.season4.homework4.task2;

import java.util.*;
import java.util.function.Predicate;

public interface TaskManager {

    public void addTask(Date date, Task task);

    public void removeTask(Date date);

    public Set<String> getCategories();

    public Map<String, List<Task>> getTasksByCategories();

    public List<Task> getTasksByCategory(String category);

    public List<Task> getTasksForToday();

    public List<Task> filterTasks(Predicate<Task> predicate);
}
