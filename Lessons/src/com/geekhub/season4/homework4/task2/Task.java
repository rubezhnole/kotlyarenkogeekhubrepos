package com.geekhub.season4.homework4.task2;

public class Task {

    private String category;
    private String description;

    public Task(String category, String description) {
        this.category = category;
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "category: " + getCategory() + " description: " + getDescription();
    }
}
