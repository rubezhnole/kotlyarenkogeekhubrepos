package com.geekhub.season4.homework4.taskOne;

import java.util.Set;

public interface SetOperations<T> {

    public boolean equals(Set<T> a, Set<T> b);

    public Set<T> union(Set<T> a, Set<T> b);

    public Set<T> subtract(Set<T> a, Set<T> b);

    public Set<T> intersect(Set<T> a, Set<T> b);

    public Set<T> symmetricSubtract(Set<T> a, Set<T> b);
}
