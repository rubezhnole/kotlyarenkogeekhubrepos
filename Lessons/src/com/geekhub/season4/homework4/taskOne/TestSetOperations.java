package com.geekhub.season4.homework4.taskOne;

import java.util.HashSet;
import java.util.Set;

public class TestSetOperations<T> implements SetOperations<T> {

    @Override
    public boolean equals(Set<T> a, Set<T> b) {

        if (a.size() != b.size()) {
            return false;
        }

        for (T tmp : a) {
            if (!b.contains(tmp)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public Set<T> union(Set<T> a, Set<T> b) {
        Set<T> res = new HashSet<>();

        for (T tmp : a) {
            res.add(tmp);
        }

        for (T tmp : b) {
            res.add(tmp);
        }

        return res;
    }

    @Override
    public Set<T> subtract(Set<T> a, Set<T> b) {
        Set<T> result = new HashSet<>();

        for (T i : a) {

            if (!b.contains(i)) {
                result.add(i);
            }
        }

        return result;
    }

    @Override
    public Set<T> intersect(Set<T> a, Set<T> b) {
        Set<T> res = new HashSet<>();

        for (T tmp : a) {
            for (T tmp2 : b) {
                if (tmp.equals(tmp2)) {
                    res.add(tmp);
                }
            }
        }

        return res;
    }

    @Override
    public Set<T> symmetricSubtract(Set<T> a, Set<T> b) {
        Set<T> res;

        res = union(subtract(a, b), subtract(b, a));

        return res;
    }

    public static void main(String[] args) {

        Set<Integer> set1 = new HashSet<Integer>();

        for (int i = 0; i < 50; i++) {
            set1.add((int) (Math.random() * 50));
        }

        Set<Integer> set2 = new HashSet<Integer>();

        for (int i = 0; i < 50; i++) {
            set2.add((int) (Math.random() * 50));
        }

        TestSetOperations to = new TestSetOperations();

        boolean result = to.equals(set1, set2);

        System.out.println(result + " equals\n");

        Set<Integer> resultSet = to.union(set1, set2);

        for (Integer i : resultSet) {
            System.out.println(i + " union");
        }

        System.out.println();

        Set<Integer> resultSet1 = to.subtract(set1, set2);

        for (Integer i : resultSet1) {
            System.out.println(i + " subtract");
        }

        System.out.println();

        Set<Integer> resultSet2 = to.intersect(set1, set2);

        for (Integer i : resultSet2) {
            System.out.println(i + " intersect");
        }

        System.out.println();

        Set<Integer> resultSet3 = to.symmetricSubtract(set1, set2);

        for (Integer i : resultSet3) {
            System.out.println(i + " symmetric subtract");
        }

        System.out.println();

        Set<String> str = new HashSet<>();
        str.add("a");
        str.add("b");
        str.add("c");

        Set<String> str2 = new HashSet<>();
        str2.add("c");
        str2.add("d");
        str2.add("e");

        Set<String> rers = to.symmetricSubtract(str, str2);

        for (String s : rers) {
            System.out.println(s + " string symmetric subtract");
        }

        Set<Integer> resInt = to.symmetricSubtract(set1, set2);

        for (Integer i : resInt) {
            System.out.println(i + " integer symmetric substract");
        }

    }
}
