package com.geekhub.season4.homework5;

import com.geekhub.season4.homework5.source.SourceLoader;
import com.geekhub.season4.homework5.source.URLSourceProvider;

import java.io.IOException;
import java.util.Scanner;

public class TranslatorController {

    public static void main(String[] args) {
        //initialization
        SourceLoader sourceLoader = new SourceLoader();
        Translator translator = new Translator(new URLSourceProvider());

        Scanner scanner = new Scanner(System.in);
        String command = scanner.next();
        while(!"exit".equals(command)) {

            String source = null;
            String translation = null;

            try {
                source = sourceLoader.loadSource(command);
                translation = translator.translate(source);
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("Original: " + source);
            System.out.println("Translation: " + translation);

            command = scanner.next();
        }
    }
}
