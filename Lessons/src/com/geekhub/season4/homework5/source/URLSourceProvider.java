package com.geekhub.season4.homework5.source;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Implementation for loading content from specified URL.<br/>
 * Valid paths to load are http://someurl.com, https://secureurl.com, ftp://frpurl.com etc.
 */
public class URLSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {

        try (InputStream url = new URL(pathToSource).openStream()){

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public String load(String pathToSource) throws IOException {

        String s;
        StringBuilder sb = new StringBuilder();

        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        new URL(pathToSource).openStream()))) {

            while ((s = in.readLine()) != null) {
                sb.append(s).append("\n");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        //For reading data from yandex api you could use UrlSourceProvider.

        return sb.toString();
    }
}
